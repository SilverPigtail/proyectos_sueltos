/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pdo;

import java.util.Scanner;

/*
por 30xp:

-Añadir a la clase Persona un String "puesto". Usarlo solo para valores "ordeño","carniceria" o "gerencia".
-Añadir a la clase Persona un Float "sueldo", que expresará el sueldo en euros.
-añadir un método cuantoGana a la clase Persona, que devuelva el String "Gana"+sueldo+" €"
-añadir un método proporcionSueldo a la clase Persona, que reciba una persona por argumentos, y devuelva un float con cuántas veces gana esa persona el sueldo de this.
-Añadir un método ajustaSueldoA, que reciba un objeto Persona, y asigne el sueldo de this, poniéndolo en proporción al del objeto que recibe por parámetro. Un/a ordeñador/a debería ganar un 25% más que alguien de carnicería, y un/a gerente un 50% más que alguien de ordeño.
-Crear la clase Vaca, con Strings para nombre, número de serie y función ("ordeño" o "carniceria"), un Float peso, un int edad, y un array de float con litros ordeñados en el día.
-Crear un método asignaFunción a una vaca, de forma que si pesa más de 200kg, se asigne a carnicería, y si pesa menos, a ordeño.
-Añadir a la clase persona un Array de Vacas
-Crear en la clase persona un método quedarmeVacas(Vaca[] vac), que copie en el array interno a las vacas que corresponden a su puesto de trabajo.*/

public class PDO {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        int a=3;
        Integer b=new Integer(3);
        Scanner sc=new Scanner(System.in);
        Persona p1=new Persona();
        p1.nombre="Miguel";
        p1.edad=32;
        p1.altura=1.71f;
        p1.apellidos="Páramos";
        p1.sueldo=1600;
        p1.puesto="gerente";
        
        Persona p2=new Persona();
        p2.nombre="Lola";
        p2.apellidos="Mento";
        p2.sueldo=700;
        p2.puesto="carnicero";
        
        
        ///
        Vaca v1=new Vaca();
        Vaca v2=new Vaca();
        
        v1.peso=200;
        
       
        v1.asignafuncion();
              
        
        System.out.println(
                p2.imprimePersona()
        );
        
    
        System.out.println(
            p1.proporcionSueldo(p2));
    
        System.out.println(
                p1.ajustasueldoA(p1)
                
        );
    

    }
    
    
        
    
        
    

}