/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pdo;

public class Persona {
    String nombre;
    String apellidos;
    int edad;
    float altura;
    String puesto;
    float sueldo;
    
    int[] vacas=new int[20];
    
        
    public String imprimePersona(){
        return this.nombre+":"+this.apellidos+":"+this.edad+":"+this.altura;
        //No importa escribir el this porque java asume que está ahí.
        //Puedes escogerlo si quieres, apenas hay diferencia.
    }
    /***
     * Dice si la persona que llama a la función es mayor de una cierta edad
     * @param int edad la edad con la que vamos a comparar a la persona
     * @return true si es mayor, false si es menor
     */
    public boolean esMayorDe(int edad){
        return this.edad>edad;
    }
    
    public String cuantoGana(){
        return "Gana "+this.sueldo+"€.";
    }
    
    public float proporcionSueldo(Persona per){
       
        System.out.println(this.sueldo);
        System.out.println(per.sueldo);
        return this.sueldo/per.sueldo;
        
    }
    
    //Añadir un método ajustaSueldoA, que reciba un objeto Persona, y asigne el sueldo de this,
    //poniéndolo en proporción al 
    //del objeto 
    //que recibe por parámetro. Un/a ordeñador/a debería ganar un 25% más que
    //alguien de carnicería, y un/a gerente un 50% más que alguien de ordeño.

    
    public String ajustasueldoA(Persona per){
        
        float subida=(float)0;
     
   if(this.puesto.equals("ordeñador")){
        if(this.puesto.equals("gerencia")){
           this.sueldo=(float)(sueldo*0.50);
        }else if(this.puesto.equals("carnicero")){
           this.sueldo=(float)(sueldo*0.25);
        }
   }else if(this.puesto.equals("gerencia")){
           this.sueldo=(float)(sueldo*1.50);
        if(this.puesto.equals("carnicero")){
           this.sueldo=(float)(sueldo*1.25);
        } 
   }else if(this.puesto.equals("carniceria")){
       if(this.puesto.equals("gerencia")){
            this.sueldo=(float)(sueldo/1.50);
        }else if(this.puesto.equals("carnicero")){
            this.sueldo=(float)(sueldo/1.25);
        }     
   }
    return this.sueldo+"€";
    
    }
    
    
    
    
    
}
